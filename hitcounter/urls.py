from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'hitcounter.views.home', name='home'),
    url(r'^ping/$', 'hitcounter.views.ping', name='ping'),
    url(r'^clear/$', 'hitcounter.views.clear', name='clear'),
    url(r'^status/$', 'hitcounter.views.status', name='status'),
    # url(r'^hitcounter/', include('hitcounter.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
