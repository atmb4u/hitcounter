# -*- coding: utf-8 *-*

from django.http import HttpResponse
from datetime import datetime, timedelta


def home(request):
    """
    Home - track incoming requests, and respond with statistics
    """
    from redis import Redis
    ip_address = request.META['REMOTE_ADDR']
    redis_client = Redis("localhost")
    if redis_client.get(ip_address):
        count = redis_client.incr(ip_address)
    else:
        redis_client.set(ip_address, 1)
        count = 1
    redis_client.rpush(ip_address + "_visits", str(datetime.now()))
    visit_list = redis_client.lrange(ip_address + "_visits", 0, -1)
    if visit_list:
        last_visit = datetime.strptime(visit_list[-1], '%Y-%m-%d %H:%M:%S.%f')
        if last_visit + timedelta(hours=1) > datetime.now():
            if len(visit_list) > 2:
                previous_visit = datetime.strptime(visit_list[-2], '%Y-%m-%d %H:%M:%S.%f')
                time_delta = datetime.now() - previous_visit
                discount = time_delta.seconds / 36 # second*100/60*60
                discount = discount+(count/10)  # also depends on 10% of number of visits
                return HttpResponse("We have" + str(discount) + "% discount specially for you Mr." + ip_address + "(visits:"+ str(count)+" )")
    return HttpResponse(" Welcome" + ip_address+ "Total no. of visits" + str(count) + "<br>" + str(visit_list))


def clear(request):
    try:
        from redis import Redis
        ip_address = request.META['REMOTE_ADDR']
        redis_client = Redis("localhost")
        redis_client.delete(ip_address)
        redis_client.delete(ip_address + "_visits")
        return HttpResponse("Success")
    except:
        return HttpResponse("Failure")


def ping(request):
    try:
        from redis import Redis
    except:
        return HttpResponse("Python-Redis binding not installed properly. Try pip install redis")
    try:
        ip_address = request.META['REMOTE_ADDR']
        redis_client = Redis("localhost")
        return HttpResponse("Pong"+ ip_address)
    except:
        return HttpResponse("Redis not installed properly. try sudo apt-get install redis-server ( in ubuntu) or goto redis.io for ")


def status(request):
    from redis import Redis
    ip_address = request.META['REMOTE_ADDR']
    redis_client = Redis("localhost")
    visit_list = redis_client.lrange(ip_address + "_visits", 0, -1)
    return HttpResponse(str(visit_list))
